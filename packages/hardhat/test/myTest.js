const { ethers } = require("hardhat");
const { use, expect } = require("chai");
const { solidity } = require("ethereum-waffle");

use(solidity);

describe("My Dapp", function () {
  let registryContract;

  describe("HandshakeDomainRegistry", function () {
    it("Should deploy HandshakeDomainRegistry", async function () {
      const [owner] = await ethers.getSigners();
      const LinkToken = await ethers.getContractFactory("LinkToken");
      const OracleOperator = await ethers.getContractFactory("Operator");
      const HandshakeDomainRegistry = await ethers.getContractFactory("HandshakeDomainRegistry");

      const MyLinkToken = await LinkToken.deploy()
      console.log('link token contract', MyLinkToken.address);
      const MyOracle = await OracleOperator.deploy(MyLinkToken.address);
      console.log('oracle contract', MyOracle.address);

      registryContract = await HandshakeDomainRegistry.deploy(
        MyOracle.address,
        MyLinkToken.address,
        ethers.utils.formatBytes32String('mychainlinkjobid'),
        MyOracle.address // tld issuer stub
      );
      // console.log('registry contract', registryContract.interface, registryContract.address);
    });

    describe("registerNewClaim()", function () {
      it("Should emit event on a new claim", async function () {
        const preEvents = await registryContract.queryFilter('ClaimRegistered');
        expect(preEvents).to.be.empty;
        const newDomain= "eventDomain";
        const [claimer] = await ethers.getSigners();
        await registryContract.registerNewClaim(claimer.address, newDomain);
        const postEvents = await registryContract.queryFilter('ClaimRegistered');
        expect(postEvents).to.have.lengthOf(1);
      });

      it("Should be able to set register new claim on a domain", async function () {
        const newDomain= "testdomain";
        const [claimer] = await ethers.getSigners();
        await registryContract.registerNewClaim(claimer.address, newDomain);
        const events = await registryContract.queryFilter('ClaimRegistered');
        const { claimer: claimerAddress, tld } = events[1].args;
        expect(claimerAddress).to.equal(claimer.address);
        expect(tld).to.equal(newDomain);
      });

      it("Should generate a deterministic nonce for a new claim", async function () {
        const newDomain= "testdomain";
        const [claimer] = await ethers.getSigners();
        await registryContract.registerNewClaim(claimer.address, newDomain);
        const events = await registryContract.queryFilter('ClaimRegistered');
        const { nonce } = events[1].args;
        const claimBlock = await events[2].getBlock();
        const { utils: { keccak256, defaultAbiCoder } } = ethers;
        const deterministicNonce = keccak256(defaultAbiCoder.encode(
          ['address', 'uint256'],
          [claimer.address, claimBlock.timestamp]
        ));
        expect(nonce).to.equal(deterministicNonce);
      });

      // should fail when contract paused

    });
    describe("requestClaimVerification()", function () {
      // should fail if no claim fee sent
      // should deduct claim fee from sender
      // should deduct LINK tokens == oracleFee  from registry contract
      // should fail if tld already claimed
      // should fail when contract paused
    });
    describe("receiveClaimVerification()", function() {
      // how to test receiving the callback???

      // do approve/deny testing here since they are internal
    });
  });
});
