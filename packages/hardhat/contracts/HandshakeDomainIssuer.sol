pragma solidity ^0.7.0;

import '@openzeppelin/contracts/access/Ownable.sol';
import './HNSTLDNFT.sol';

contract HandshakeDomainIssuer is Ownable {
  HNSTLDNFT public TLDTokenContract;

  mapping(address => bool) allowedRegistries;
  mapping(address => bool) pausedRegistries;
  //  store NFT contract for upgradability?

  constructor(address _owner, address _tldTokenContract) {
    // how toset owner?
    TLDTokenContract = HNSTLDNFT(_tldTokenContract);
  }

  function issue(string calldata domain, address namer)
    external
    onlyAllowedRegistries(msg.sender)
    returns (uint256 tokenId)
  {
    // issue nft
    uint256 id = TLDTokenContract.mint(domain, namer);
    return id;
  }

  function updateRegistryPermission (address registry, bool permission) public onlyOwner returns (bool) {
    allowedRegistries[registry] = permission;
    return permission;
  }

  function updateRegistryPause (address registry, bool paused) public onlyOwner returns (bool) {
    pausedRegistries[registry] = paused;
    return paused;
  }

  modifier onlyAllowedRegistries(address registry) {
    require(allowedRegistries[registry], 'Only allowed registries');
    _;
  }

  modifier onlyUnpausedRegistries(address registry) {
    require(!pausedRegistries[registry], 'Only unpaused registries');
    _;
  }
}
