// SPDX-License-Identifier: MIT
pragma solidity ^0.7.0;

import '@openzeppelin/contracts/access/Ownable.sol';
import '@openzeppelin/contracts/utils/Pausable.sol';
import "@chainlink/contracts/src/v0.7/ChainlinkClient.sol";
import "./HandshakeDomainIssuer.sol";

contract HandshakeDomainRegistry is ChainlinkClient, Ownable, Pausable {
  address private hnsOracle;
  bytes32 private verifyTldJobId;
  address private link;
  uint256 private oracleFee = 0.1 * 10 ** 18; // 0.1 LINK per verification request
  string constant registrySubdomain = 'xnhns'; // subdomain where nonce is stored to prove ownership of on tld being claimed
  string constant noncePrefix = 'xnhns-domain-verification='; // nonce prefix on TXT record to specify application

  // private uint256 MAX_CLAIMS = 
  address public tldIssuer;

  uint256 private claimsCounter;
  uint256 private claimFee = 0.5 * 10 ** 18;

  event ClaimRegistered (uint256 indexed claimId, address claimer, string tld, bytes32 nonce);
  event ClaimApproved (uint256 indexed claimId, address claimer, string tld);
  event ClaimDenied (uint256 indexed claimId, address claimer, string tld);
  event TldRevoked (address claimer, string tld);

  struct Claim {
    address claimer;
    string tld;
    bytes32 nonce;
  }

  mapping(string => uint256) tldTokens;
  mapping(uint256 => Claim) claims;
  mapping(bytes32 => uint256) claimForRun;

  constructor(address _hnsOracle, address _link, bytes32 _verifyTldJobId, address _tldIssuer) {
    link = _link;
    tldIssuer = _tldIssuer;
    hnsOracle = _hnsOracle;
    verifyTldJobId = _verifyTldJobId;
    claimsCounter = 0;
  }

  function registerNewClaim(address claimer, string calldata tld)
    public
    whenNotPaused
    returns (uint256)
  {
    // require(claimsCounter < MAX_CLAIMS, "max tlds claimed in registry");
    require(tldTokens[tld] == 0, "Domain already claimed");

    bytes32 nonce = _generateClaimNonce();
    claimsCounter++;
    claims[claimsCounter] = Claim({
      claimer: claimer,
      nonce: nonce,
      tld: tld
    });

    emit ClaimRegistered(claimsCounter, claimer, tld, nonce);
    return claimsCounter;
  }

  function requestClaimVerification(uint256 claimId)
    public
    payable
    whenNotPaused
    returns (bytes32)
  {
    require(msg.value >= claimFee, 'Insufficient claim fee sent');
    Claim memory claim = claims[claimId];
    require(tldTokens[claim.tld] == 0, "Domain already claimed");
    msg.sender.transfer(claimFee);
    // require(LINK.balanceOf(address(this)) >= fee, "Not enough LINK for request");
    Chainlink.Request memory request = buildChainlinkRequest(
      verifyTldJobId,
      address(this),
      this.receiveClaimVerification.selector
    );

    Chainlink.add(request, "domain", claim.tld);
    Chainlink.add(request, "registrySubdomain", registrySubdomain);
    Chainlink.add(request, "nonce", _bytes32ToString(claim.nonce));
    Chainlink.add(request, "noncePrefix", noncePrefix);

    // save claim to request id so we can update claim on response
    claimForRun[request.id] = claimId;

    return sendChainlinkRequestTo(hnsOracle, request, oracleFee);
  }

  function receiveClaimVerification(bytes32 requestId, bool isVerified)
    public
    recordChainlinkFulfillment(requestId)
  {
    uint256 claimId = claimForRun[requestId];

    if(isVerified) {
      approveClaim(claimId);
    } else {
      denyClaim(claimId);
    }
  }

  function approveClaim(uint256 claimId) whenNotPaused internal {
      Claim memory claim = claims[claimId];
      require(claim.claimer != address(0), 'No claim registered'); // checking claim isn't null, not actual address
      emit ClaimApproved(claimId, claim.claimer, claim.tld);
      uint256 tldTokenId = tldTokens[claim.tld];

      // if verifid but no token, issue tld token to claimer
      if(tldTokenId == 0) { 
        issueTld(claim);
      }
  }

  function denyClaim(uint256 claimId) internal {
      Claim memory claim = claims[claimId];
      require(claim.claimer != address(0), 'No claim registered'); // checking claim isn't null, not actual address
      emit ClaimDenied(claimId, claim.claimer, claim.tld);

      // if previously verified but no longer valid revoke tld from owner
      if(tldTokens[claim.tld] != 0) {
        revokeTld(claim);
      }
  }

  function issueTld(Claim memory claim) internal {
    uint256 id = tldIssuer.issue(claim.tld, claim.claimer, claim.nonce);
    // tldTokens[domain] = id;
  }

  function revokeTld(Claim memory claim) internal {
    uint256 tldTokenId = tldTokens[claim.tld];
    tldTokens[claim.tld] = 0; // remove tld token from registry
    // burn tld token
    emit TldRevoked(claim.claimer, claim.tld);
    // would be nice to payout claimFee*2 to address that called requestVerification as a keeper system
    // or would current owner of tld alwaysbe incentivized to call it so that they can claim the tld themselves?
  }

  /* Helper functions */

  function _bytes32ToString(bytes32 _bytes32) public pure returns (string memory) {
    uint8 i = 0;
    while(i < 32 && _bytes32[i] != 0) {
        i++;
    }
    bytes memory bytesArray = new bytes(i);
    for (i = 0; i < 32 && _bytes32[i] != 0; i++) {
        bytesArray[i] = _bytes32[i];
    }
    return string(bytesArray);
  }


  function _generateClaimNonce() internal view returns (bytes32) {
    return keccak256(abi.encode(msg.sender, block.timestamp));
  }

  /* Admin functions */

  function updateOracle(address _hnsOracle, bytes32 _jobId, uint256 _fee)
    public
    onlyOwner
    whenPaused // must stop service to update oracles
  {
    require(_hnsOracle != address(0), 'must update oracle');
    hnsOracle = _hnsOracle;
    verifyTldJobId = _jobId;
    oracleFee = _fee;
  }

  function pause() public onlyOwner {
    _pause();
  }

  function unpause() public onlyOwner {
    _unpause();
  }
}
