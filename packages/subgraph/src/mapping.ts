import { BigInt, Address, log } from "@graphprotocol/graph-ts"
import {
  HandshakeDomainRegistry,
  ClaimRegistered,
  ClaimApproved,
  ClaimDenied,
  ClaimRevoked,
} from "../generated/HandshakeDomainRegistry/HandshakeDomainRegistry"
import { Claim, Domain } from "../generated/schema"

export function handleClaimRegistered(event: ClaimRegistered): void {
  // log(event.params)
  let claimId = event.params.claimId.toHex()
  let claimer = event.params.claimer
  let tld = event.params.tld
  let nonce = event.params.nonce
  let c = Claim.load(claimId)
  let d = Domain.load(tld)
  

  if(!c) {
    c.nonce = nonce
    c.claimer = claimer
    c.domain = tld
    c.approved = false
    c.revoked = false
  }
  
  if(!d) {
    d.domain = tld
  }

  c.save()
  d.save()
}



export function handleClaimApproved(event: ClaimApproved): void {
  // log(event.params)
  let claimId = event.params.claimId.toHex()
  let tld = event.params.tld
  let c = Claim.load(claimId)
  let d = Domain.load(tld)
  c.approved = true
  c.approvedAt = event.block.timestamp
  d.ownershipClaim = claimId
  d.save()
  c.save()
}

export function handleClaimDenied(event: ClaimDenied): void {
  // log(event.params)
  let claimId = event.params.claimId.toHex()
  let c = Claim.load(claimId)
  c.approved = false
  c.save()
}

export function handleClaimRevoked(event: ClaimRevoked): void {
  // log(event.params)
  let claimId = event.params.claimId.toHex()
  let tld = event.params.tld
  let c = Claim.load(claimId)
  let d = Domain.load(tld)
  c.approved = false
  c.revoked = true
  c.revokedAt = event.block.timestamp
  d.ownershipClaim = null
  c.save()
}
