import React from "react";
import { parseEther, formatEther } from "@ethersproject/units";

export default function NewClaimVerification({
  nonce,
  domain
}) {
  return (
    <div>
      Make sure you added a TXT record with a value of
      `xnhns-domain-verification={nonce}` to your .{domain}/ TLD

      Our <a href="https://chain.link" target="_blank"> ChainLink </a> oracle
      will verify your TXT record is present then respond back to the Ethereum blockchain to issue a token representing your domain.
      <h3> TLD Claiming Fee: {formatEther(0.01)} </h3>
      <button onClick={}> Verify TLD Ownership </button>
    </div>
  );
}
