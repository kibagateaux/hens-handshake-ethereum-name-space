/* eslint-disable jsx-a11y/accessible-emoji */

import React, { useState } from "react";
import { Button, List, Divider, Input, Card, DatePicker, Slider, Switch, Progress, Spin } from "antd";
import { SyncOutlined } from '@ant-design/icons';
import { Address, Balance } from "../components";
import { parseEther, formatEther } from "@ethersproject/units";

export default function ExampleUI({
  claims,
  ClaimRegisteredEvents,
  ClaimDeniedEvents,
  ClaimApprovedEvents,
  address,
  mainnetProvider,
  localProvider,
  userProvider,
  yourLocalBalance,
  price,
  tx,
  readContracts,
  writeContracts
}) {

  const [newClaimDomain, setNewClaimDomain] = useState("loading...");
  const [newClaimNonce, setNewClaimNonce] = useState(null);

  return (
    <div>
      {/*
        ⚙️ Here is an example UI that displays and sets the purpose in your smart contract:
      */}
      <div style={{border:"1px solid #cccccc", padding:16, width:400, margin:"auto",marginTop:64}}>
        <h2>Handshake Registry on Ethereum</h2>
        <p> Claim your Handshake TLD to supercharge your domain business </p>


        <h4>claims: {claims}</h4>

        <Divider/>

        <div style={{margin:8}}>
          <Input placeholder='.influencer/' onChange={(e)=>{setNewClaimDomain(e.target.value)}} />
          <Button onClick={()=>{
            console.log("newClaimDomain",newClaimDomain)
            /* look how you call registerNewClaim on your contract: */
            tx( writeContracts.HandshakeDomainRegistry.registerNewClaim(address, newClaimDomain) )
          }}>Claim TLD</Button>
        </div>

        <Divider />
        
        Address To Claim Domain To:
        <Address
            value={address}
            ensProvider={userProvider}
            fontSize={16}
        />

        <Divider/>

        {  /* use formatEther to display a BigNumber: */ }
        <h2>Your Balance: {yourLocalBalance?formatEther(yourLocalBalance):"..."}</h2>

        <Divider/>

        Handshake Registry Contact Address:
        <Address
            value={readContracts?readContracts.HandshakeDomainRegistry.address:readContracts}
            ensProvider={userProvider}
            fontSize={16}
        />

        <Divider />
      </div>

      {/*
        📑 Maybe display a list of events?
          (uncomment the event and emit line in HandshakeDomainRegistry.sol! )
      */}
      <div style={{ width:600, margin: "auto", marginTop:32, paddingBottom:32 }}>
        <h2>Events:</h2>
        <List
          bordered
          dataSource={ClaimRegisteredEvents}
          renderItem={(item) => {
            return (
              <List.Item key={item.blockNumber+"_"+item.sender+"_"+item.purpose}>
                <Address
                    value={item[0]}
                    ensProvider={userProvider}
                    fontSize={16}
                  /> =>
                {item[1]}
              </List.Item>
            )
          }}
        />
      </div>


      <div style={{ width:600, margin: "auto", marginTop:32, paddingBottom:256 }}>

        <Card>

          Check out all the <a href="https://github.com/austintgriffith/scaffold-eth/tree/master/packages/react-app/src/components" target="_blank" rel="noopener noreferrer">📦  components</a>

        </Card>

        <Card style={{marginTop:32}}>

          <div>
            There are tons of generic components included from <a href="https://ant.design/components/overview/" target="_blank" rel="noopener noreferrer">🐜  ant.design</a> too!
          </div>

          <div style={{marginTop:8}}>
            <Button type="primary">
              Buttons
            </Button>
          </div>

          <div style={{marginTop:8}}>
            <SyncOutlined spin />  Icons
          </div>

          <div style={{marginTop:8}}>
            Date Pickers?
            <div style={{marginTop:2}}>
              <DatePicker onChange={()=>{}}/>
            </div>
          </div>

          <div style={{marginTop:32}}>
            <Slider range defaultValue={[20, 50]} onChange={()=>{}}/>
          </div>

          <div style={{marginTop:32}}>
            <Switch defaultChecked onChange={()=>{}} />
          </div>

          <div style={{marginTop:32}}>
            <Progress percent={50} status="active" />
          </div>

          <div style={{marginTop:32}}>
            <Spin />
          </div>


        </Card>




      </div>


    </div>
  );
}
